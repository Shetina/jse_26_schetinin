package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    Boolean isLoginExist(@NotNull String login);

    Boolean isEmailExist(@NotNull String email);

}
